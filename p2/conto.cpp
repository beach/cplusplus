/*
Ho quattro conti:
- ContoBancario, che ha un saldo, op di prelievo e deposito; (conviene agg il getSaldo)
- ContoCorrente che è un contobancario con una tassa fissa per tutti gli oggetti sui depositi e sui prelievi
- ContoDiRIsparmio, che è un contobancario ma non ha tasse, e non può andare in rosso (occhio ai prelievi);
- ContoArancio, che eredita da ContoDiRisparmio e ha un ContoCorrente di appoggio(sup);
        + un deposito/prelievo sul COntoArancio comporta un prelievo/deposito, rispettivamente, sul ContoCorrente
*/

#include <iostream>
using std::cout; 
using std::endl;

class ContoBancario{
private:
	double saldo;
public:
	void Deposita(double);
	void Preleva(double);
	double GetSaldo()const;
	ContoBancario();
};

void ContoBancario::Deposita(double amount){
	saldo= saldo+ amount;
}

void ContoBancario::Preleva(double amount){
	saldo= saldo- amount;
}

double ContoBancario::GetSaldo()const{
	return saldo;
}

ContoBancario::ContoBancario(): saldo(0){}

//====================================================================

class ContoCorrente: public ContoBancario{
private:
	//eredita saldo
	static const double tax= 0.2;
public:
	void Deposita(double);
	void Preleva(double);
	//eredita GetSaldo
	//niente costru
};

void ContoCorrente::Deposita(double amount){
	ContoBancario::Deposita(amount);
	ContoBancario::Preleva(tax);

}

void ContoCorrente::Preleva(double amount){
	ContoBancario::Preleva(amount);
	ContoBancario::Preleva(tax);
}

//ContoCorrente::ContoCorrente():tax(0.1){}

//====================================================================

class ContoDiRisparmio: public ContoBancario{
private:
	//eredita saldo
public:
	//eredita deposita
	void Preleva(double);
	//eredita GetSaldo
};



void ContoDiRisparmio::Preleva(double amount){
	if( ContoBancario::GetSaldo()- amount < 0 )
		cout<<"Impossibile effettuare il prelievo: andresti in rosso di: "<<ContoBancario::GetSaldo()-amount << endl;
	else 
		ContoBancario::Preleva(amount);
}

//====================================================================

/*
ContoAramcio ha un contocorrente di appoggio:
- quando depositi una somma S su contoarancio, prelievi anche dal contocorrente;
- i prelievi  di S da contoarancio vengono depositati sul contocorrente
*/


class ContoArancio: public ContoDiRisparmio{
private:
	ContoCorrente sup;
public:
	void Deposita(double);
	void Preleva(double);
	//eredita GetSaldo
	double GetSaldoSup() const;
};

void ContoArancio::Deposita(double amount){
	ContoBancario::Deposita(amount);
	sup.ContoCorrente::Preleva(amount);
}

void ContoArancio::Preleva(double amount){
	if( ContoBancario::GetSaldo()- amount < 0 )
		cout<<"Impossibile effettuare il prelievo: andresti in rosso di: "<<ContoBancario::GetSaldo()-amount << endl;
	else 
	{
		ContoBancario::Preleva(amount);
		sup.ContoCorrente::Deposita(amount);
	}
}

double ContoArancio::GetSaldoSup()const{
	return sup.GetSaldo();
}


//====================================================================

int main(){
	//ContoBancario a;
	//ContoCorrente b;
	//ContoDiRisparmio c;
	ContoArancio d;
	cout<<"saldo: "<<d.GetSaldo()<<" ; saldo sup: " <<d.GetSaldoSup()<<endl;
	d.Deposita(5);
	cout<<"saldo: "<<d.GetSaldo()<<" ; saldo sup: " <<d.GetSaldoSup()<<endl;
	d.Deposita(100);
	cout<<"saldo: "<<d.GetSaldo()<<" ; saldo sup: " <<d.GetSaldoSup()<<endl;
	d.Preleva(3);
	cout<<"saldo: "<<d.GetSaldo()<<" ; saldo sup: " <<d.GetSaldoSup()<<endl;
	d.Preleva(100);
	cout<<"saldo: "<<d.GetSaldo()<<" ; saldo sup: " <<d.GetSaldoSup()<<endl;
	//a.Preleva(3.0);
	//cout<<	b.GetSaldo()<<endl;
	//c.Deposita(10);
	//c.Deposita(15.2);
	//c.Preleva(5.2);
	//c.Preleva(22);
	
	//cout<<a.GetSaldo()<<endl;

}