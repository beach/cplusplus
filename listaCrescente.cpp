#include <iostream>
 
using namespace std;
 
struct nodo {
    int info;
    nodo *next;
    nodo (int a=0,nodo* b=NULL)
    {info=a;next=b;}
};
 
 
nodo* inserisci_in_ordine(int k, nodo *L)
{
    if (!L)
			return new nodo(k,NULL);
		
		if(k<L->info)
			return new nodo(k,L);
		
    else // k>=L->info
		{
		 	L->next=inserisci_in_ordine(k, L->next);
    	return L;
    }
}
 
void stampa (nodo* L)
{
    if(L)
    {
    	cout<<L->info<<' '; 
    	stampa(L->next);
    }
}
 
 
int main()
{
    cout << "start" << endl;
    int i;
    nodo* F= NULL;
    cin >> i;
    while (i!=-1){
        F= inserisci_in_ordine(i,F);
        cin>>i;
    }
   
    stampa(F);
 
    cout << "end" << endl;
}
