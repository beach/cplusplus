/*
Il programma legge le parole di un testo e le inserisce in una tabella. 
La tabella e` un array di table. table e` una struttura fatta di un intero, che indica la quantita`,e una stringa, che corrisponde alla parola.

Il programma e` migliorabile, e di molto. Si puo` usare una tabella dinamica. Si dovrebbe allora modificare dimensioni e controlli nelle funzioni. Si dovrebbe controllare fino a quando si trova la fine del file (EOF) o della tabella.

Si potrebbero anche ordinare le parole nella tabella, in ordine alfabetico. Serve analizzare i singoli caratteri delle stringhe per ordinarle. Questo potrebbe essere un esercizio a parte.

*/




#include <iostream>
#include <fstream>
#include <string>
using namespace std;

struct table{
	int q; //quantity
	string n; //name
	table(int a=0, string b="empty")
		{q=a;n=b;}
};

struct nodo{
	int info;
	nodo*next;
	nodo(int a=0, nodo*b=0){info=a; next=b;}
	
};

//PRE: ho almeno un posto dove inserire il nuovo nome
void insert(table* T, string name){
	bool exit=false;
	for(int i=0; i<30 && exit==false; i++){
		if(T[i].q==0) //ok
		{
			T[i].q=1;
			T[i].n= name;
			exit=true;
		}
	}
	if(exit==false) cout<<"errore 'insert'\n";
}
//POST: aggiunto name in T, q corrispondente =1

// cerca match di 'name' in 'table'
bool match(table*T, string name){
	bool flag= false;
	for(int i=0; i<30 && T[i].q!=0; i++){ //controllo, solo celle non vuote
		if(T[i].n== name)
		{ //match
			T[i].q++;
			flag=true;
		}
	}
	return flag;
}
//POST: ritorna il val di flag: true se ho matchato, false se nessun match

void add(){}

void print(table*T){
	for(int i=0; i<30 && T[i].q!=0 ; i++){
		cout<<T[i].q <<' ' <<T[i].n <<endl;
	}
}


main(){
	ifstream in("input");
	if(in){
		string a;
		table T[30]; //max 30 parole
		
		for(int i=0; a!="."; i++){ //input word
			in>>a;
			if(a!=".") 
			{
				if(!match(T,a))//parola gia` inserita?
					insert(T,a); //no? allora, nuovo inserimento
			}
		}
		print(T);
		
	}
	else cout<<"errore input\n";
	

} //END MAIN
